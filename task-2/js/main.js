
const checkField = (name,phone) => {
		let tmp = {
			success: true,
			msg: '',
		}
		if(!name.value.length) {
			tmp.msg += 'Поле имени не должнго быть пустым ';
			tmp.success = false;
		}
		if(!/^\+?(\d+\-?\d+)+$/.test(phone.value)) {
			tmp.msg += 'Телефон должени содержать цифры, + или - ';
			tmp.success = false;
			
		}
		return tmp;
}

let Person = Backbone.Model.extend({
	defaults: {
		name: 'Vasil',
		phone: '899922022',
		has_error: false,
		msg: ''
	},
	validate(attrs) {
	
	}
});


let PeopleCollection = Backbone.Collection.extend({
	model: Person
});

let PeopleView = Backbone.View.extend({
	tagName: 'div',
	el: '#people',
	initialize() {
		this.collection.on('add',this.showPerson, this);
		this.collection.on('invalid',this.invalidAttrs, this);
		this.collection.on('change',this.render, this);
	},
		
	render() {
		this.$el.html('');
		this.collection.each(this.showPerson,this)
	},

	showPerson(item) {
			let person = new PersonView({model: item});
			this.$el.append(person.render().el);
	}
})

let AddPersonView =  Backbone.View.extend({
	el: '#header',
	events: {
		'click #person-add-btn': 'addPersonToList'
	},
	addPersonToList() {
		const name = document.getElementById('person-name-input');
		const phone = document.getElementById('person-phone-input');
		let header_error = document.getElementById('header-error');
		const result = checkField(name,phone);
		if(!result.success) {
			header_error.innerHTML = result.msg;
			header_error.classList.remove('hidden');
		}else {
			header_error.classList.add('hidden');
			let newPerson = new Person({name: name.value,phone: phone.value});
			this.collection.add(newPerson);
		}
	},
	initialize() {

	}
});

let PersonView = Backbone.View.extend({

	initialize(){
		this.model.on('destroy',this.remove,this);

	},

	render(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	},
	tagName: 'div',
	className: 'person-row',
	template: _.template(document.getElementById('person-template').innerHTML),
	events: {
		'click .person-delete': 'deletePerson',
		'click .person-edit': 'editPerson',
		'click .btn-edit-save': 'saveEdit'
	},
	deletePerson() {
		this.model.destroy();
	},
	remove() {
		this.$el.remove();
	},
	editPerson(e) {
		let parent = e.target.closest('.person-row');
		let personName = parent.querySelector('.person-name');
		let personPhone = parent.querySelector('.person-phone');
		let nameInput = parent.querySelector('#inputName');
		let phoneInput = parent.querySelector('#inputPhone');
		personName.classList.add('hidden');
		personPhone.classList.add('hidden');
		nameInput.classList.remove('hidden');
		phoneInput.classList.remove('hidden');
		let btn = parent.querySelector('#edit-btn');
		btn.innerHTML = 'Сохранить';
		btn.classList.add('btn-edit-save');
	},
	saveEdit(e) {
		let parent = e.target.closest('.person-row');
		let nameInput = parent.querySelector('#inputName');
		let phoneInput = parent.querySelector('#inputPhone');
		let result = checkField(nameInput,phoneInput);
			let personName = parent.querySelector('.person-name');
			let personPhone = parent.querySelector('.person-phone');
			this.model.set({
				name: nameInput.value,
				phone: phoneInput.value,
				msg: result.msg,
				has_error: !result.success
			});
			personName.classList.remove('hidden');
			personPhone.classList.remove('hidden');
			nameInput.classList.add('hidden');
			phoneInput.classList.add('hidden');
			let btn = parent.querySelector('#edit-btn');
			btn.innerHTML = 'Редактировать';
			btn.classList.remove('btn-edit-save');
	}

});

let people = new PeopleCollection([
{
	name: 'Leha'
},
{
	name: 'Yura'
},
{
	name: 'Valera'
},
{
	name: 'Julia'
}
]);


let showall = new PeopleView({collection: people});
let addPersonView = new AddPersonView({collection: people});
showall.render();

