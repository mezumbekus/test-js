
/**
*** Основные методы
**/

const drawStar = (ctx,params) => {
	if(!ctx)
		throw new Error('Не получилось получить context');
	const defaultParams = {
		cX: 50,
		cY: 50,
		radius: 50,
		color: 'red'
	}
	const resultParams = {
		...defaultParams,
		...params
	}
	const {cX,cY,radius,color} = resultParams;
	ctx.beginPath();
	ctx.moveTo(cX + radius,cY);
 	for(let i = 1; i <= 10; i++)
 	{
 		if(i % 2 == 0){
 		var theta = i * (Math.PI * 2) / (10);
 		var x = cX + (radius * Math.cos(theta));
 		var y = cY + (radius * Math.sin(theta));
 		} else {
 		var theta = i * (Math.PI * 2) / (10);
 		var x = cX + ((radius/2) * Math.cos(theta));
 		var y = cY + ((radius/2) * Math.sin(theta));
 	}
 	ctx.lineTo(x ,y);
 	}
 	ctx.fillStyle = color;
 	ctx.fill();
 	ctx.closePath();
}

const initStars = (container,N,func,colors) => {
	if(!container)
		throw new Error('Необходимо передать канвас контейнер');
	const ctx = container.getContext('2d');
	ctx.fillStyle = '#fff';
	ctx.fillRect(0,0,ctx.canvas.width,ctx.canvas.height);
	const colorsMaxIndex = colors.length;
	const stepX = 100;
	let startPonitX = 50;
	for(let i = 0; i < N; i++) {
		const color = {
			color: colors[i%colorsMaxIndex],
			cX: startPonitX
		}
		func(ctx,color);
		startPonitX+=stepX;
	}
}

const rgbToHex = (r,g,b) => {
	const _r = r < 10 ? '0'+Number(r).toString(16): Number(r).toString(16);
	const _g = g < 10 ? '0'+Number(g).toString(16):Number(g).toString(16);
	const _b = b < 10 ? '0'+Number(b).toString(16):Number(b).toString(16);
	return '#'+_r+_g+_b;
}

const setColorToSubCanvas = (ctx,color) => {
	ctx.canvas.style.backgroundColor = color;
	/* Второй способ
	ctx.fillStyle = color;
	ctx.fillRect(0,0,ctx.canvas.width,ctx.canvas.height);*/
}

const onClickStar = (e) => {
	const ctx = e.target.getContext('2d');
	const colors = ctx.getImageData(e.clientX,e.clientY,1,1).data;
	const canvasSub = document.getElementById('stars-sub');
	const ctxSub = canvasSub.getContext('2d');
	const color = rgbToHex(colors[0],colors[1],colors[2]);
	setColorToSubCanvas(ctxSub,color);
}

const canvasMain = document.getElementById('stars-main');
initStars(canvasMain,5,drawStar,['red','blue','green','yellow','black']);

/**
*** События
**/

canvasMain.addEventListener('click',onClickStar);





